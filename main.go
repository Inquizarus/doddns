package main

import "gitlab.com/inquizarus/doddns/cmd/doddns/app"

func main() {
	app.Execute()
}
