package app

import "github.com/spf13/cobra"

func makeRootCmd(commands ...*cobra.Command) *cobra.Command {
	rootCmd := &cobra.Command{
		Use: "doddns",
		Run: func(cmd *cobra.Command, args []string) {},
	}
	return rootCmd
}
