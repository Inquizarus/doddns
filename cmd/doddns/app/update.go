package app

import (
	"net/http"

	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/doddns/pkg/do"
	"gitlab.com/inquizarus/doddns/pkg/logwrapper"
	"gitlab.com/inquizarus/doddns/pkg/lookup"
	"gitlab.com/inquizarus/doddns/pkg/net"
)

func makeUpdateCmd(logger *logwrapper.StandardLogger, domainsLoader func() ([]net.TargetDomain, error)) *cobra.Command {
	cmd := &cobra.Command{
		Use:  "update",
		Long: "updates the remote IP of passed target domains A record with this units external IP",
		Run: func(cmd *cobra.Command, args []string) {
			var ip net.IP
			var err error

			domains, err := domainsLoader()

			if nil != err {
				logger.Error(err)
				return
			}

			externalIPServiceURL, err := cmd.Flags().GetString("external-ip-service")

			if nil != err {
				logger.Error(err)
				return
			}

			externalIPClient := lookup.DefaultExternalIPClient{
				URL:        externalIPServiceURL,
				HTTPClient: http.DefaultClient,
			}

			localIPClient := lookup.DefaultLocalIPClient{}

			client := do.DefaultClient{
				AccessToken: accessToken,
			}

			ip, err = externalIPClient.Get()

			if nil != err {
				logger.Errorf(`could not resolve external IP, reason: %s`, err.Error())
				return
			}

			localIP, err := localIPClient.Get()

			if nil != err {
				logger.Errorf(`could not resolve local IP, reason: %s`, err.Error())
				return
			}

			updateDomains(domains, ip, localIP, client, logger)

		},
	}

	return cmd
}

func updateDomains(domains []net.TargetDomain, externalIP net.IP, localIP net.IP, client do.DefaultClient, logger *logwrapper.StandardLogger) {
	for _, targetDomain := range domains {
		client.TargetDomain = targetDomain.Name
		for _, targetDomainRecord := range targetDomain.Records {
			targetIP := externalIP
			if true == targetDomainRecord.UseLocal {
				targetIP = localIP
			}
			logger.Infof(`will be using ip %s when updating %s`, targetIP.Value, targetDomainRecord.Name)
			err := client.UpdateARecordIP(targetDomainRecord, targetIP)
			if nil != err {
				logger.Errorf(`could not update ip for %s, reason: %s`, targetDomainRecord.Name, err.Error())
			}
		}
	}
}
