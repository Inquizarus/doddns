package app

import (
	"fmt"
	"os"
	"os/user"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/inquizarus/doddns/pkg/logwrapper"
	"gitlab.com/inquizarus/doddns/pkg/net"
)

var cfgFile string
var rootCmd = makeRootCmd()
var accessToken string
var domainsData string

// Execute ...
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.doddns.yaml)")
	rootCmd.PersistentFlags().StringVarP(&accessToken, "access-token", "a", "", "access token for Digitalocean API")
	rootCmd.PersistentFlags().StringP("external-ip-service", "e", "https://api.ipify.org", "URL to service to use when retrieving public IP address")
	rootCmd.PersistentFlags().StringVarP(&domainsData, "domains-data", "d", "", "json string of domains data to use")
	viper.BindPFlag("config", rootCmd.Flags().Lookup("config"))
	viper.BindPFlag("access-token", rootCmd.Flags().Lookup("access-token"))
	viper.BindPFlag("external-ip-service", rootCmd.Flags().Lookup("external-ip-service"))
	viper.BindPFlag("domains-data", rootCmd.Flags().Lookup("domains-data"))
	logger := logwrapper.NewStandardLogger()

	loadDomains := func() ([]net.TargetDomain, error) {
		return net.TargetDomainsFromRawJSONString(domainsData)
	}

	rootCmd.AddCommand(makeUpdateCmd(logger, loadDomains), makeWatchCmd(logger, loadDomains))
}

func initConfig() {
	if cfgFile != "" {
		viper.SetConfigFile(cfgFile)
	} else {
		usr, err := user.Current()
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
		viper.AddConfigPath(usr.HomeDir)
		viper.SetConfigName(".doddns")
	}
	viper.SetEnvPrefix("doddns")
	viper.ReadInConfig()
}

func loadDomainsFile(fp string, rfn func(filename string) ([]byte, error)) ([]net.TargetDomain, error) {
	var domains []net.TargetDomain
	var err error
	bbytes, err := rfn(fp)
	if nil != err {
		return domains, nil
	}
	return net.TargetDomainsFromBytes(bbytes)
}
