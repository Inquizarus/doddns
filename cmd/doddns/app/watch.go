package app

import (
	"net/http"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/inquizarus/doddns/pkg/do"
	"gitlab.com/inquizarus/doddns/pkg/logwrapper"
	"gitlab.com/inquizarus/doddns/pkg/lookup"
	"gitlab.com/inquizarus/doddns/pkg/net"
)

func makeWatchCmd(logger *logwrapper.StandardLogger, domainsLoader func() ([]net.TargetDomain, error)) *cobra.Command {
	watchCmd := &cobra.Command{
		Use:  "watch",
		Long: "watches and updates the remote IP of passed target domains A record with this units external IP",
		Run: func(cmd *cobra.Command, args []string) {
			domains, err := domainsLoader()

			if nil != err {
				logger.Error(err)
				return
			}

			externalIPServiceURL, err := cmd.Flags().GetString("external-ip-service")

			if nil != err {
				logger.Error(err)
				return
			}

			externalIPClient := lookup.DefaultExternalIPClient{
				URL:        externalIPServiceURL,
				HTTPClient: http.DefaultClient,
			}
			localIPClient := lookup.DefaultLocalIPClient{}
			client := do.DefaultClient{
				AccessToken: accessToken,
			}
			i, err := cmd.Flags().GetInt("interval")
			if nil != err {
				logger.Error(err)
				return
			}
			interval := time.NewTicker(time.Duration(i) * time.Second)
			logger.Info(`starting watch`)
			for {
				select {
				case <-interval.C:
					logger.Info(`starting check if DNS need to be updated`)
					ip, err := externalIPClient.Get()
					if nil != err {
						logger.Errorf(`could not resolve external IP, reason: %s`, err.Error())
						continue
					}
					localIP, err := localIPClient.Get()
					if nil != err {
						logger.Errorf(`could not resolve local IP, reason: %s`, err.Error())
						continue
					}
					updateDomains(domains, ip, localIP, client, logger)
					logger.Infof("done with DNS check, sleeping for %d seconds", i)
				}
			}
		},
	}
	watchCmd.Flags().IntP("interval", "i", 60, "determine how often the watch will trigger an update")
	return watchCmd
}

func checkAndUpdate(eip lookup.ExternalIPClient, c do.Client, n string) error {
	ip, err := eip.Get()
	if nil == err {
		err = c.UpdateARecordIP(n, ip)
	}
	return err
}
