package logwrapper

import (
	"github.com/sirupsen/logrus"
)

// StandardLogger enforces better logging
type StandardLogger struct {
	*logrus.Logger
}

// NewStandardLogger initializes the standard logger
func NewStandardLogger() *StandardLogger {
	baseLogger := logrus.New()
	standardLogger := &StandardLogger{baseLogger}
	standardLogger.Formatter = &logrus.JSONFormatter{}
	return standardLogger
}
