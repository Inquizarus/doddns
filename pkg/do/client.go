package do

import (
	"context"
	"fmt"

	"github.com/digitalocean/godo"
	"gitlab.com/inquizarus/doddns/pkg/net"
	"golang.org/x/oauth2"
)

// Client interface for wrapping Digitalocean calls
type Client interface {
	UpdateARecordIP(string, net.IP) error
}

// DefaultClient for using godo in Digitalocean calls
type DefaultClient struct {
	client       godo.Client
	AccessToken  string
	TargetDomain string
}

func (dc *DefaultClient) getGoDoClient() *godo.Client {
	oauthClient := oauth2.NewClient(context.Background(), &tokenSource{
		AccessToken: dc.AccessToken,
	})
	return godo.NewClient(oauthClient)
}

// UpdateARecordIP updates remote IP with passed one
func (dc *DefaultClient) UpdateARecordIP(r net.TargetRecord, ip net.IP) error {
	var err error
	client := dc.getGoDoClient()
	record, err := dc.getRecord(r.Name, r.Type)
	if nil != err {
		return err
	}
	remoteIP := net.IP{Value: record.Data}
	if remoteIP.IsSame(ip) {
		return fmt.Errorf("remote and current IP is the same, no change will be made")
	}
	_, _, err = client.Domains.EditRecord(
		context.Background(),
		dc.TargetDomain,
		record.ID,
		&godo.DomainRecordEditRequest{
			Name:     record.Name,
			Flags:    record.Flags,
			Port:     record.Port,
			Priority: record.Priority,
			TTL:      record.TTL,
			Tag:      record.Tag,
			Type:     record.Type,
			Weight:   record.Weight,
			Data:     ip.Value,
		},
	)

	return err
}

func (dc *DefaultClient) getRecord(name string, t string) (godo.DomainRecord, error) {
	var correct godo.DomainRecord
	var err error
	records, _, err := dc.getGoDoClient().Domains.Records(context.Background(), dc.TargetDomain, &godo.ListOptions{})
	if nil == err {
		for _, record := range records {
			if t == record.Type && name == record.Name {
				correct = record
				break
			}
		}
		if "" == correct.Name {
			err = fmt.Errorf("could not find record with name %s", name)
		}
	}
	return correct, err
}
