package lookup

import (
	osnet "net"

	"gitlab.com/inquizarus/doddns/pkg/net"
)

// LocalIPClient interface to lookup current units internal ip
type LocalIPClient interface {
	IPClient
}

// DefaultLocalIPClient for getting your local network ip
type DefaultLocalIPClient struct{}

// Get retrieves the IP from local interface
func (c *DefaultLocalIPClient) Get() (net.IP, error) {
	var err error
	conn, err := osnet.Dial("udp", "8.8.8.8:80")
	defer conn.Close()
	localAddr := conn.LocalAddr().(*osnet.UDPAddr)
	return net.IP{Value: localAddr.IP.String()}, err
}
