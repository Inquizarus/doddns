package lookup

import (
	"io/ioutil"
	"net/http"

	"gitlab.com/inquizarus/doddns/pkg/net"
)

// ExternalIPClient interface to lookup current units external ip
type ExternalIPClient interface {
	IPClient
}

// DefaultExternalIPClient that uses external websites to find it
type DefaultExternalIPClient struct {
	HTTPClient *http.Client
	URL        string
}

// Get retrieves the external IP from URL
func (c *DefaultExternalIPClient) Get() (net.IP, error) {
	var ip net.IP
	var err error
	r, err := c.HTTPClient.Get(c.URL)
	if nil != err {
		return ip, err
	}
	bbytes, err := ioutil.ReadAll(r.Body)
	ip.Value = string(bbytes)
	return ip, err
}
