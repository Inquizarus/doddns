package lookup

import "gitlab.com/inquizarus/doddns/pkg/net"

// IPClient is the generic interface for retrieving an IP address from some source
type IPClient interface {
	Get() (net.IP, error)
}
