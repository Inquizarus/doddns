package net

import "encoding/json"

// IP ...
type IP struct {
	Value string
}

// IsSame compares two IP addresses and determine if they are the same
func (ip *IP) IsSame(other IP) bool {
	return ip.Value == other.Value
}

func (ip *IP) String() string {
	return ip.Value
}

// DomainsFile represents a file containing information about domains
// that should be updated.
type DomainsFile struct {
	Domains []TargetDomain `json:"domains"`
}

// TargetRecord represents a record which will be dynamically
// updated under a domain
type TargetRecord struct {
	Type     string `json:"type"`
	Name     string `json:"name"`
	UseLocal bool   `json:"use-local"`
}

// TargetDomain represents a domain which will have records
// dynamically updated
type TargetDomain struct {
	Name    string         `json:"name"`
	Records []TargetRecord `json:"records"`
}

// TargetDomainsFromBytes helps to build up the structs from input bytes array
func TargetDomainsFromBytes(b []byte) ([]TargetDomain, error) {
	var domainsFile DomainsFile
	var err error
	err = json.Unmarshal(b, &domainsFile)
	return domainsFile.Domains, err
}

// TargetDomainsFromRawJSONString helps to transform a json string into
// TargetDomains
func TargetDomainsFromRawJSONString(s string) ([]TargetDomain, error) {
	var domains []TargetDomain
	var err error
	raw := json.RawMessage(s)
	domainsBytes, err := raw.MarshalJSON()
	if nil == err {
		domains, err = TargetDomainsFromBytes(domainsBytes)
	}
	return domains, err
}
