# DODDNS

Small program that will update Digitalocean DNS entries with either local or public ip for the machine
it is running on when it detect that whatever value is registered does not match what it expects it to be.


## Flags

short | long | description
---------|----------|---------
 a | access-token | Your Digitalocean access token with permissions to edit DNS entries
 e | external-ip-service | The service which will be used to retrieve the machines public IP address
 d | domains-data | The JSON data that will be used to determine what will be updated 


## Domains data
JSON blob that defines which domains and DNS records that should be updated.
```json
{
    "domains": [
        {
            "name": "domain.example",
            "records": [
                {
                    "type": "A",
                    "name": "x"
                },
                {
                    "type": "MX",
                    "name": "y"
                }
            ]
        }
    ]
}
```

#### Domain
**Name**: Domain name of which DNS records are registered under.

**Records**: List of records for the site that should be dynamically maintained.

#### Record
**Type**: DNS record type like *A/SRV/MX*.

**Name**: Name of the record to update

## Download latest releases

[regular](https://gitlab.com/Inquizarus/doddns/-/jobs/artifacts/master/raw/builds/doddns?job=compile)

[amd64](https://gitlab.com/Inquizarus/doddns/-/jobs/artifacts/master/raw/builds/doddns_amd64?job=compile)

[arm6](https://gitlab.com/Inquizarus/doddns/-/jobs/artifacts/master/raw/builds/doddns_arm6?job=compile)